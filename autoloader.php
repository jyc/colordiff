<?php
/**
 * Registers an (almost) PSR-0 compliant autoloader and adds the directory two levels up (the PSR-0 "vendors" directory)
 * to the include path.
 *
 * @package ColorDiff
 * @author Jonathan Chan <jchan@icebrg.us>
 * @copyright 2011 Jonathan Chan <jchan@icebrg.us>
 * @license http://www.opensource.org/licenses/bsd-license.php The BSD 2-Clause License
 */

spl_autoload_register(function ($className) {
    require __DIR__ . '/../../' . str_replace('_', '/',
            str_replace('\\', '/', ltrim($className, '\\')) . '.php');
});

/* End of File autoloader.php */