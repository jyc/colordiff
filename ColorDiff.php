<?php
/**
 * @package ColorDiff
 * @author Jonathan Chan <jchan@icebrg.us>
 * @copyright 2011 Jonathan Chan <jchan@icebrg.us>
 * @license http://www.opensource.org/licenses/bsd-license.php The BSD 2-Clause License
 */
namespace icebrg\colordiff;

use InvalidArgumentException;

/**
 * Facilitates calculation of the difference between two colors.
 *
 * @author Jonathan Chan <jchan@icebrg.us>
 */
class ColorDiff
{
    /**
     * Converts an RGB color to an (L*, a*, b*) color.
     * @static
     * @param array $r A tuple of R, G, and B components.
     * @return array A tuple of l*, a*, and b* components.
     * @throws \InvalidArgumentException
     */
    public static function rgbToLab(array $r)
    {
        if (count($r) !== 3) {
            throw new InvalidArgumentException("\$r must contain R, G, and B values (0-255).");
        }

        // Taken from http://www.easyrgb.com/index.php?X=MATH.

        $r[0] = $r[0] / 255;        //R from 0 to 255
        $r[1] = $r[1] / 255;        //G from 0 to 255
        $r[2] = $r[2] / 255;        //B from 0 to 255

        foreach ($r as &$v) {
            if ($v > 0.04045) {
                $v = pow(($v + 0.055) / 1.055, 2.4);
            } else {
                $v = $v / 12.92;
            }
        }

        $r[0] = $r[0] * 100;
        $r[1] = $r[1] * 100;
        $r[2] = $r[2] * 100;

        // $x = XYZ values
        $x = array();
        // Observer = 2 deg, Illuminant = D65
        $x[0] = $r[0] * 0.4124 + $r[1] * 0.3576 + $r[2] * 0.1805;
        $x[1] = $r[0] * 0.2126 + $r[1] * 0.7152 + $r[2] * 0.0722;
        $x[2] = $r[0] * 0.0193 + $r[1] * 0.1192 + $r[2] * 0.9505;

        $x[0] = $x[0] / 95.047;
        $x[1] = $x[1] / 100.0;
        $x[2] = $x[2] / 108.883;

        foreach ($x as &$v) {
            if ($v > 0.008856) {
                $v = pow($v, 1 / 3);
            } else {
                $v = (7.787 * $v) + (16 / 116);
            }
        }

        // $l = CIE-L*ab values
        $l = array();
        $l[0] = (116 * $x[1]) - 16;
        $l[1] = 500 * ($x[0] - $x[1]);
        $l[2] = 200 * ($x[1] - $x[2]);

        return $l;
    }

    /**
     * Converts an (L*, a*, b) color to an RGB color.
     * @static
     * @param array $l A tuple of l*, a*, and b* components.
     * @return array A tuple of R, G, and B components.
     * @throws \InvalidArgumentException
     */
    public static function labToRgb(array $l)
    {
        if (count($l) !== 3) {
            throw new InvalidArgumentException("\$l must contain R, G, and B values (0-255).");
        }

        // Taken from http://www.easyrgb.com/index.php?X=MATH.

        $x = array();
        $x[1] = ($l[0] + 16) / 116;
        $x[0] = $l[1] / 500 + $x[1];
        $x[2] = $x[1] - $l[2] / 200;

        foreach ($x as &$v) {
            if (pow($v, 3) > 0.008856) {
                $v = pow($v, 3);
            } else {
                $v = ($v - 16 / 116) / 7.787;
            }
        }

        $x[0] = 95.047 * $x[0];
        $x[1] = 100.0 * $x[1];
        $x[2] = 108.883 * $x[2];

        $x[0] = $x[0] / 100;
        $x[1] = $x[1] / 100;
        $x[2] = $x[2] / 100;

        $r = array();
        $r[0] = $x[0] * 3.2406 + $x[1] * -1.5372 + $x[2] * -0.4986;
        $r[1] = $x[0] * -0.9689 + $x[1] * 1.8758 + $x[2] * 0.0415;
        $r[2] = $x[0] * 0.0557 + $x[2] * -0.2040 + $x[2] * 1.0570;

        foreach ($r as &$v) {
            if ($v > 0.0031308) {
                $v = 1.055 * pow($v, 1 / 2.4) - 0.055;
            } else {
                $v = 12.92 * $v;
            }
        }

        $r[0] = $r[0] * 255;
        $r[1] = $r[1] * 255;
        $r[2] = $r[2] * 255;

        return $r;
    }

    /**
     * Calculates the difference between two colors using the CIEDE2000 color difference formula.
     * @static
     * @param array $l1 A tuple of L*, a*, and b* components.
     * @param array $l2 A tuple of L*, a*, and b* components.
     * @return float The calculated difference, ranging from 0 to ~100.
     * @throws \InvalidArgumentException
     * @see http://en.wikipedia.org/wiki/Color_difference#CIEDE2000
     */
    public static function ciede2000(array $l1, array $l2)
    {
        if (count($l1) !== 3) {
            throw new InvalidArgumentException("\$l1 must contain L*, a*, and b* values (-128 to 128).");
        }

        if (count($l2) !== 3) {
            throw new InvalidArgumentException("\$l2 must contain L*, a*, and b* values (-128 to 128).");
        }

        list($L1, $a1, $b1) = $l1;
        list($L2, $a2, $b2) = $l2;

        // d = delta
        // p = prime
        // c1 = c subscript 1
        // Avg = average

        $C1 = sqrt(pow($a1, 2) + pow($b1, 2));
        $C2 = sqrt(pow($a2, 2) + pow($b2, 2));
        $dLp = $L2 - $L1;
        $Lavg = ($L1 + $L2) / 2;
        $Cavg = ($C1 + $C2) / 2;
        $a1p = $a1 + ($a1 / 2) * (1 - sqrt(pow($Cavg, 7) / (pow($Cavg, 7) + pow(25, 7))));
        $a2p = $a2 + ($a2 / 2) * (1 - sqrt(pow($Cavg, 7) / (pow($Cavg, 7) + pow(25, 7))));
        $C1p = sqrt(pow($a1p, 2) + pow($b1, 2));
        $C2p = sqrt(pow($a2p, 2) + pow($b2, 2));
        $CavgP = ($C1p + $C2p) / 2;
        $dCp = $C1p - $C2p;
        $h1p = $a1p ? atan($b1 / $a1p) % (2 * pi()) : 0; // guard against / by 0
        $h2p = $a2p ? atan($b2 / $a2p) % (2 * pi()) : 0;

        $dhp = null;
            if (abs($h1p - $h2p) <= pi()) {
                $dhp = $h2p - $h1p;
            } elseif (abs($h1p - $h2p) > pi() && $h2p <= $h1p) {
                $dhp = $h2p - $h1p + 2 * pi();
            } elseif (abs($h1p - $h2p) > pi() && $h2p > $h1p) {
                $dhp = $h2p - $h1p - 2 * pi();
            } // whew

        $dHp = 2 * sqrt($C1p * $C2p) * sin($dhp / 2);

        $HavgP = null;
            if (abs($h1p - $h2p) > pi()) {
                $HavgP = ($h1p + $h2p + 2 * pi()) / 2;
            } elseif (abs($h1p - $h2p) <= pi()) {
                $HavgP = ($h1p + $h2p) / 2;
            }

        $T = 1 - 0.17 * cos($HavgP - pi() / 6) + 0.24 * cos(2 * $HavgP) + 0.32 * cos(3 * $HavgP + pi() / 30) - 0.20 * cos(4 * $HavgP - 21 * pi() / 60);
        $Sl = 1 + (0.015 * pow($Lavg - 50, 2)) / sqrt(20 + pow($Lavg - 50 ,2));
        $Sc = 1 + 0.045 * $CavgP;
        $Sh = 1 + 0.015 * $CavgP * $T;
        $Rt = -2 * sqrt(pow($CavgP, 7) / (pow($CavgP, 7) + pow(25, 7))) * sin(pow(pi() / 6, -1 * pow(($HavgP - 275) / 25, 2)));

        return sqrt(pow($dLp / $Sl, 2) + pow($dCp / $Sc, 2) + pow($dHp / $Sh, 2) + $Rt * ($dCp / $Sc) * ($dHp / $Sh));
    }
}

/* End of File ColorDiff.php */