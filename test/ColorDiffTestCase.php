<?php
/**
 * @package ColorDiff
 * @author Jonathan Chan <jchan@icebrg.us>
 * @copyright 2011 Jonathan Chan <jchan@icebrg.us>
 * @license http://www.opensource.org/licenses/bsd-license.php The BSD 2-Clause License
 */
namespace icebrg\colordiff\test;

require_once __DIR__ . '/../autoloader.php';

use icebrg\colordiff\ColorDiff;

/**
 * @author Jonathan Chan <jchan@icebrg.us>
 */
class ColorDiffTestCase extends \PHPUnit_Framework_TestCase
{
    public function testCiede2000()
    {
        $this->assertEquals(0, ColorDiff::ciede2000(
            ColorDiff::rgbToLab(array(0, 0, 0)),
            ColorDiff::rgbToLab(array(0, 0, 0))),
            "The difference between rgb(0, 0, 0) and rgb(0, 0, 0) should be 0."
        );

        $this->assertEquals(100, round(ColorDiff::ciede2000(
            ColorDiff::rgbToLab(array(0, 0, 0)),
            ColorDiff::rgbToLab(array(255, 255, 255))), 3),
            "The difference between rgb(0, 0, 0) and rgb(255, 255, 255) should be ~100 (floating point)."
        );
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testRgbToLabValidation()
    {
        ColorDiff::rgbToLab(array(false, false));
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testLabToRgbValidation()
    {
        ColorDiff::labToRgb(array(false, false));
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testCiede2000Validation()
    {
        ColorDiff::ciede2000(array(false), array(false));
    }

    public function testRgbToLab()
    {
        $this->assertEquals(array(0, 0, 0), ColorDiff::rgbToLab(array(0, 0, 0)),
            "rgb(0, 0, 0) should convert to lab(0, 0, 0)."
        );

        $lab = ColorDiff::rgbToLab(array(255, 255, 255));

        $this->assertEquals(100, round($lab[0]), "rgb(255, 255, 255) should convert to L* ~100.");
        $this->assertEquals(0, round($lab[1]), "rgb(255, 255, 255) should convert to a* ~100.");
        $this->assertEquals(0, round($lab[1]), "rgb(255, 255, 255) should convert to b* ~100.");
    }
}

/* End of File ColorDiffTestCase.php */